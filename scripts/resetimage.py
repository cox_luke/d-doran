import boto3
import io
from botocore.exceptions import ClientError

landing = "data-landing-dev-20220127163408341800000002"
key = "images/IMG_1.jpg"
session = boto3.session.Session(profile_name='devl')
s3 = session.resource('s3')
s3.Object("data-landing-dev-20220127163408341800000002", 'images/IMG_1.jpg').delete()

file_binary = open("/Users/danieldoran/Documents/dsi-infrastructure/terraform/deployments/sitcen-data-pipelines/scripts/images/IMG_1.jpg", "rb").read()
file_as_binary = io.BytesIO(file_binary)
try:
    s3_client.upload_fileobj(file_as_binary,landing, key)
except ClientError as e:
    print(e)
