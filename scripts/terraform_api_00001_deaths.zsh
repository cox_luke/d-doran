#!/bin/zsh
 terraform apply  \
--target=aws_glue_catalog_table.api_00001_deaths_landing \
--target=aws_s3_bucket_object.api_00001_deaths_extract_script \
--target=aws_glue_job.api_00001_deaths_extract \
--target=aws_glue_catalog_table.api_00001_deaths_data_repository \
--target=aws_s3_bucket_object.api_00001_deaths_transform_script \
--target=aws_glue_job.api_00001_deaths_transform \
 --target=aws_glue_workflow.api_00001_deaths \
 --target=aws_glue_trigger.api_00001_deaths_extract \
 --target=aws_glue_trigger.api_00001_deaths_transform